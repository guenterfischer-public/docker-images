#!/bin/bash

# Script to install the Zephyr-SDK
#
# See following references for details:
# - https://docs.zephyrproject.org/latest/develop/getting_started/index.html#
# - https://github.com/zephyrproject-rtos/sdk-ng/tags

ZEPHYR_SDK_VERSION=0.16.8
ZEPHYR_SDK_DIRECTORY=/home/docker/zephyr-sdk-${ZEPHYR_SDK_VERSION}

cd /tmp
wget https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v${ZEPHYR_SDK_VERSION}/zephyr-sdk-${ZEPHYR_SDK_VERSION}_linux-x86_64.tar.xz
wget -O - https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v${ZEPHYR_SDK_VERSION}/sha256.sum | shasum --check --ignore-missing

mkdir -p ${ZEPHYR_SDK_DIRECTORY}
tar xvf zephyr-sdk-${ZEPHYR_SDK_VERSION}_linux-x86_64.tar.xz -C ${ZEPHYR_SDK_DIRECTORY} --strip-components=1
rm zephyr-sdk-${ZEPHYR_SDK_VERSION}_linux-x86_64.tar.xz

cd ${ZEPHYR_SDK_DIRECTORY}
chmod +x ./setup.sh
yes | ./setup.sh

# Do some clean-up to reduce image size
rm -rf ${ZEPHYR_SDK_DIRECTORY}/aarch64-zephyr-elf
rm -rf ${ZEPHYR_SDK_DIRECTORY}/arc64-zephyr-elf
rm -rf ${ZEPHYR_SDK_DIRECTORY}/arc-zephyr-elf
rm -rf ${ZEPHYR_SDK_DIRECTORY}/microblazeel-zephyr-elf
rm -rf ${ZEPHYR_SDK_DIRECTORY}/mips-zephyr-elf
rm -rf ${ZEPHYR_SDK_DIRECTORY}/nios2-zephyr-elf
rm -rf ${ZEPHYR_SDK_DIRECTORY}/riscv64-zephyr-elf
rm -rf ${ZEPHYR_SDK_DIRECTORY}/x86_64-zephyr-elf
rm -rf ${ZEPHYR_SDK_DIRECTORY}/sparc-zephyr-elf
rm -rf ${ZEPHYR_SDK_DIRECTORY}/sysroots
rm -rf ${ZEPHYR_SDK_DIRECTORY}/xtensa-*
find ${ZEPHYR_SDK_DIRECTORY} -type d -name ".git" | xargs rm -rf
