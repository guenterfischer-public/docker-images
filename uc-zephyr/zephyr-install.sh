#!/bin/bash

# Script to install Zephyr
#
# See following references for details:
# - https://docs.zephyrproject.org/latest/develop/getting_started/index.html#
# - https://docs.zephyrproject.org/latest/releases/index.html

ZEPHYR_VERSION=3.7.0
ZEPHYR_DIRECTORY=/home/docker/zephyrproject

west init --mr v${ZEPHYR_VERSION} ${ZEPHYR_DIRECTORY}
cd ${ZEPHYR_DIRECTORY} &&
	west update
cd ${ZEPHYR_DIRECTORY} &&
	west zephyr-export

pip3 install -r ${ZEPHYR_DIRECTORY}/zephyr/scripts/requirements.txt

# Do some clean-up to reduce image size
rm -rf ${ZEPHYR_DIRECTORY}/zephyr/.git
rm -rf ${ZEPHYR_DIRECTORY}/zephyr/doc
rm -rf ${ZEPHYR_DIRECTORY}/zephyr/tests
rm -rf ${ZEPHYR_DIRECTORY}/modules/audio
rm -rf ${ZEPHYR_DIRECTORY}/modules/bsim_hw_models
# rm -rf ${ZEPHYR_DIRECTORY}/modules/lib
rm -rf ${ZEPHYR_DIRECTORY}/modules/debug
rm -rf ${ZEPHYR_DIRECTORY}/modules/tee
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/adi
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/alter
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/altera
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/ambiq
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/atmel
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/cypress
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/espressif
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/gigadevice
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/infineon
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/intel
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/libmetal
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/microchip
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/nuvoton
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/nxp
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/openisa
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/quicklogic
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/renesas
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/silabs
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/st
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/stm32
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/ti
rm -rf ${ZEPHYR_DIRECTORY}/modules/hal/xtensa
find ${ZEPHYR_DIRECTORY}/ -type d -name ".git" | xargs rm -rf
