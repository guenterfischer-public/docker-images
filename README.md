# docker-images

This repository provides a collection of Docker containers for various development use cases.

These containers are available:
- [base](#base)
- [uc-avr](#uc-avr)
- [uc-zephyr](#uc-zephyr)

## Status

![pipeline status](https://gitlab.com/guenterfischer-public/docker-images/badges/main/pipeline.svg)

## Images

### base 

This acts as base container with basic tools, settings, etc.

#### Use Cases

- Base Container
- C++ Projects
- Python Projects
- Node.js Projects

#### Getting Started

The image can either be built by yourself or just be pulled from the container registry.

```bash
# Building the image
docker build --network=host -t base ./base

# Fetching the image from the registry
docker pull registry.gitlab.com/guenterfischer-public/docker-images/base:latest
```

[AstroNvim](https://github.com/AstroNvim/AstroNvim) is used for the Neovim configuration. \
In order to use all symbols, the appropriate [nerd font](https://www.nerdfonts.com/) has to be installed.

This can be done as follows:

```bash
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/RobotoMono.zip \
  && unzip RobotoMono.zip -d ~/.fonts \
  && fc-cache -fv \
  && rm -rf RobotoMono.zip
```

### uc-avr

This image contains tools for the programming of AVR µControllers. \
It uses the 'base' image as parent and thus has all common tools installed as well.

#### Use Cases

- AVR µC Projects

#### Getting Started

The image can either be built by yourself or just be pulled from the container registry.

```bash
# Building the image
docker build --network=host -t uc-avr ./uc-avr

# Fetching the image from the registry
docker pull registry.gitlab.com/guenterfischer-public/docker-images/uc-avr:24.03
```

### uc-zephyr

This image contains tools regarding [Zephyr](https://github.com/zephyrproject-rtos/zephyr). \
It uses the 'base' image as parent and thus has all common tools installed as well.

The installation is done according to this guideline: \
<https://docs.zephyrproject.org/latest/develop/getting_started/index.html>

#### Use Cases

- nRF µC Projects

#### Getting Started

The image can either be built by yourself or just be pulled from the container registry.

```bash
# Building the image
docker build --network=host -t uc-zephyr ./uc-zephyr

# Fetching the image from the registry
docker pull registry.gitlab.com/guenterfischer-public/docker-images/uc-zephyr:24.03
```
